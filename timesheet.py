# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'timesheet.line'

    def _get_billing_line_defaults(self, line):
        res = super(Line, self)._get_billing_line_defaults(line)

        work_obj = Pool().get(res['work']._model_name)
        res['contract'] = work_obj.get_contract(res['work'])
        return res

Line()


class TimesheetWork(ModelSQL, ModelView):
    _name = 'timesheet.work'

    def get_contract(self, work):
        return False

TimesheetWork()
